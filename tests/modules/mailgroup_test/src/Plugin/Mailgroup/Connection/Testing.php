<?php

namespace Drupal\mailgroup_test\Plugin\Mailgroup\Connection;

use Drupal\mailgroup\ConnectionBase;

/**
 * Testing connection plugin.
 *
 * @Connection(
 *   id = "testing",
 *   label = @Translation("Testing"),
 *   fields_to_encrypt = {}
 * )
 */
class Testing extends ConnectionBase {

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    $config = $this->configuration;

    $fields['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $config['username'] ?? NULL,
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function testConnection() {
    return TRUE;
  }

}
