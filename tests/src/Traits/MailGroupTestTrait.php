<?php

namespace Drupal\Tests\mailgroup\Traits;

/**
 * Common functionality for mail group tests.
 */
trait MailGroupTestTrait {

  /**
   * User with admin permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * User with permission to use mail groups.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $groupUser;

  /**
   * User with no additional permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $basicUser;

  /**
   * Create test users with varying permissions.
   */
  protected function createUsers() {
    $this->adminUser = $this->drupalCreateUser([
      'administer mail groups',
    ]);

    $this->groupUser = $this->drupalCreateUser([
      'add mail groups',
      'edit mail groups',
      'delete mail groups',
      'view mail groups',
    ]);

    $this->basicUser = $this->drupalCreateUser();
  }

}
