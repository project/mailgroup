<?php

namespace Drupal\Tests\mailgroup\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\mailgroup\Entity\MailGroupInterface;
use Drupal\Tests\mailgroup\Traits\MailGroupTestTrait;
use Drupal\user\UserInterface;

/**
 * Base class for kernel tests.
 */
abstract class KernelTestBase extends EntityKernelTestBase {
  use MailGroupTestTrait;

  /**
   * The modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'key',
    'encrypt',
    'options',
    'mailgroup',
    'mailgroup_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('mailgroup');
    $this->installEntitySchema('mailgroup_membership');
    $this->installEntitySchema('mailgroup_message');
    $this->installConfig(['mailgroup', 'mailgroup_test']);

    $this->createUsers();
  }

  /**
   * Create a mail group.
   *
   * @return \Drupal\mailgroup\Entity\MailGroupInterface
   *   The mail group entity.
   */
  protected function createGroup() {
    $storage = $this->entityTypeManager->getStorage('mailgroup');
    $group = $storage->create([
      'type' => 'default',
      'name' => 'test_group',
      'email' => $this->randomMachineName() . '@example.com',
      'prepend_name' => TRUE,
      'reply_to' => 1,
      'send_type' => 1,
      'connection' => 'testing',
      'connection_config' => [
        'username' => $this->randomMachineName(),
      ],
      'status' => TRUE,
    ]);
    $group->save();

    return $group;
  }

  /**
   * Create a group membership.
   *
   * @param \Drupal\mailgroup\Entity\MailGroupInterface $group
   *   The group to add the membership to.
   * @param \Drupal\user\UserInterface $user
   *   The user to create the membership for.
   *
   * @return \Drupal\mailgroup\Entity\MailGroupMembershipInterface
   *   The membership entity.
   */
  protected function createMembership(MailGroupInterface $group, UserInterface $user) {
    $storage = $this->entityTypeManager->getStorage('mailgroup_membership');
    $membership = $storage->create([
      'gid' => $group->id(),
      'uid' => $user->id(),
      'status' => TRUE,
    ]);
    $membership->save();

    return $membership;
  }

  /**
   * Create a group message.
   *
   * @param \Drupal\mailgroup\Entity\MailGroupInterface $group
   *   The group to add the membership to.
   * @param \Drupal\user\UserInterface $user
   *   The user to create the membership for.
   *
   * @return \Drupal\mailgroup\Entity\MailGroupMessageInterface
   *   The membership entity.
   */
  protected function createMessage(MailGroupInterface $group, UserInterface $user) {
    $storage = $this->entityTypeManager->getStorage('mailgroup_message');
    $message = $storage->create([
      'gid' => $group->id(),
      'uid' => $user->id(),
      'subject' => $this->randomString(),
      'body' => $this->randomString(128),
      'status' => TRUE,
    ]);
    $message->save();

    return $message;
  }

}
