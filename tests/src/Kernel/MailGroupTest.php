<?php

namespace Drupal\Tests\mailgroup\Kernel;

use Drupal\mailgroup\Entity\MailGroupMembershipInterface;
use Drupal\mailgroup\Entity\MailGroupMessageInterface;
use Drupal\mailgroup\Exception\MailGroupInactiveException;
use Drupal\mailgroup\Exception\MemberNotFoundException;

/**
 * Tests for mail group entities.
 *
 * @group mailgroup
 */
class MailGroupTest extends KernelTestBase {

  /**
   * The mail group.
   *
   * @var \Drupal\mailgroup\Entity\MailGroupInterface
   */
  protected $group;

  /**
   * The mail group membership.
   *
   * @var \Drupal\mailgroup\Entity\MailGroupMembershipInterface
   */
  protected $membership;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->group = $this->createGroup();
  }

  /**
   * Tests getting all messages of a group.
   *
   * @covers ::getMessages
   */
  public function testGetMessages() {
    $this->createMembership($this->group, $this->groupUser);
    $this->createMessage($this->group, $this->groupUser);

    $messages = $this->group->getMessages();
    $message = reset($messages);

    $this->assertInstanceOf(MailGroupMessageInterface::class, $message);
    $this->assertEquals($this->group->id(), $message->getGroupId());
    $this->assertEquals($this->groupUser->id(), $message->getOwnerId());
  }

  /**
   * Tests getting all members of a group.
   *
   * @covers ::getMembers
   */
  public function testGetMembers() {
    $this->createMembership($this->group, $this->groupUser);

    $members = $this->group->getMembers();
    $member = reset($members);

    $this->assertInstanceOf(MailGroupMembershipInterface::class, $member);
    $this->assertEquals($this->group->id(), $member->getGroupId());
    $this->assertEquals($this->groupUser->id(), $member->getOwnerId());
  }

  /**
   * Tests getting the email addresses of all group members.
   *
   * @covers ::getMemberEmails
   */
  public function testGetMemberEmails() {
    $this->createMembership($this->group, $this->groupUser);

    $emails = $this->group->getMemberEmails();
    $email = reset($emails);

    $this->assertEquals($this->groupUser->getEmail(), $email);
  }

  /**
   * Tests checking if the owner of an email address is a member of a group.
   *
   * @covers ::isMember
   */
  public function testIsMember() {
    $this->createMembership($this->group, $this->groupUser);

    $this->assertFalse($this->group->isMember($this->basicUser->getEmail()));
    $this->assertTrue($this->group->isMember($this->groupUser->getEmail()));
  }

  /**
   * Tests fetching the membership corresponding to an email address.
   *
   * @covers ::getMember
   */
  public function testGetMember() {
    $this->createMembership($this->group, $this->groupUser);

    $this->assertNull($this->group->getMember($this->basicUser->getEmail()));
    $this->assertInstanceOf(MailGroupMembershipInterface::class, $this->group->getMember($this->groupUser->getEmail()));
  }

  /**
   * Tests an email address is allowed to send to a group.
   *
   * @covers ::isAllowedToSend
   */
  public function testIsAllowedToSend() {
    $this->createMembership($this->group, $this->groupUser);

    $this->expectException(MemberNotFoundException::class);
    $this->expectExceptionMessage("{$this->basicUser->getEmail()} is not an (active) member of group test_group.");
    $this->assertInstanceOf(MailGroupMembershipInterface::class, $this->group->isAllowedToSend($this->basicUser->getEmail()));
    $this->assertInstanceOf(MailGroupMembershipInterface::class, $this->group->isAllowedToSend($this->groupUser->getEmail()));

    $this->group->setInactive();
    $this->expectException(MailGroupInactiveException::class);
    $this->expectExceptionMessage('The group test_group is not active.');
    $this->assertInstanceOf(MailGroupMembershipInterface::class, $this->group->isAllowedToSend($this->groupUser->getEmail()));
    $this->group->setActive();
  }

}
