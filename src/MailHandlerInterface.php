<?php

namespace Drupal\mailgroup;

use Drupal\mailgroup\Entity\MailGroupMessageInterface;

/**
 * Interface for defining mail group handlers.
 */
interface MailHandlerInterface {

  /**
   * Send a message to members of the group.
   *
   * @param \Drupal\mailgroup\Entity\MailGroupMessageInterface $message
   *   The message to send.
   */
  public function sendMessage(MailGroupMessageInterface $message);

}
