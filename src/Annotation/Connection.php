<?php

namespace Drupal\mailgroup\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Connection annotation object.
 *
 * @see \Drupal\mailgroup\Plugin\ConnectionManager
 * @see \Drupal\mailgroup\Plugin\ConnectionInterface
 *
 * @Annotation
 */
class Connection extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * Configuration fields to store encrypted in the database.
   *
   * @var array
   */
  public $fields_to_encrypt;

}
