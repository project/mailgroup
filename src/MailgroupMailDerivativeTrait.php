<?php

namespace Drupal\mailgroup;

use Drupal\Component\Plugin\PluginBase;

/**
 * Contains shared functions related to derivative Mail plugins.
 */
trait MailgroupMailDerivativeTrait {

  /**
   * Build a derivative Mail interface plugin ID.
   *
   * @param string $connection_plugin_id
   *   The connection plugin ID.
   * @param string $mailgroup_id
   *   The Mail Group ID.
   *
   * @return string
   *   The derivative plugin ID.
   */
  protected function buildMailPluginDerivativeId(string $connection_plugin_id, string $mailgroup_id) {

    /*
     * From the deriver's perspective, the derivative ID is added to the Mail
     * plugin ID being derived. We have chosen to use the Mail Group ID for this
     * ID. This is done in
     * \Drupal\mailgroup\Plugin\Derivative\MailgroupMailBase::getDerivativeDefinitions.
     *
     * The mail manager however, will look for Mail plugin IDs either matching
     * a module system name or concatenation of this module system name and the
     * key passed to \Drupal\Core\Mail\MailManagerInterface::mail.
     * This means the Mail plugin derivative ID must include the module system
     * name.
     * This also means that Mail plugin IDs corresponding with Connection
     * plugins, must include the module system name in their ID.
     *
     * As a side effect, this allows one module to support multiple Connection
     * plugins with only one Mail plugin deriver class.
     * A Mail plugin for each Connection plugin is still required though.
     *
     * In summary, Mail plugins corresponding with Connection plugins that
     * support outgoing mail and need to be derived, must have a Mail plugin ID
     * as "[module system name]_[connection plugin ID]".
     *
     */
    return "{$this->getModule()}_{$this->buildMailDerivativeKey($connection_plugin_id, $mailgroup_id)}";
  }

  /**
   * Build a derivative key for mails to be sent by a derivative Mail plugin.
   *
   * This key is to be passed to \Drupal\Core\Mail\MailManagerInterface::mail.
   * In combination with the Connection plugin's module, it will ensure the
   * derivative Mail plugin is matched.
   *
   * @param string $connection_plugin_id
   *   The connection plugin ID.
   * @param string $mailgroup_id
   *   The Mail Group ID.
   *
   * @return string
   *   The key.
   */
  protected function buildMailDerivativeKey(string $connection_plugin_id, string $mailgroup_id) {
    return $connection_plugin_id . PluginBase::DERIVATIVE_SEPARATOR . $mailgroup_id;
  }

}
