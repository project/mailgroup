<?php

namespace Drupal\mailgroup\Exception;

/**
 * Thrown when trying to send a message to an inactive MailGroup.
 */
class MailGroupInactiveException extends \Exception {}
