<?php

namespace Drupal\mailgroup\Exception;

/**
 * Thrown when a membership can't be found.
 */
class MemberNotFoundException extends \Exception {}
