<?php

namespace Drupal\mailgroup\EventSubscriber;

use Drupal\mailgroup\Event\MailGroupMessageEvent;
use Drupal\mailgroup\MailHandlerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribe to mail group message events.
 */
class MailGroupMessageEventSubscriber implements EventSubscriberInterface {

  /**
   * The mail handler service.
   *
   * @var \Drupal\mailgroup\MailHandlerInterface
   */
  protected $handler;

  /**
   * Sets the mail handler service.
   *
   * @param \Drupal\mailgroup\MailHandlerInterface $mail_handler
   *   The mail handler service.
   */
  public function setHandler(MailHandlerInterface $mail_handler) {
    $this->handler = $mail_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      MailGroupMessageEvent::CREATE => 'sendMessage',
    ];
  }

  /**
   * Send a message to group members.
   *
   * @param \Drupal\mailgroup\Event\MailGroupMessageEvent $event
   *   The message event.
   */
  public function sendMessage(MailGroupMessageEvent $event) {
    /** @var \Drupal\mailgroup\Entity\MailGroupMessage $message */
    $message = $event->getMessage();

    $this->handler->sendMessage($message);
  }

}
