<?php

namespace Drupal\mailgroup\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\mailgroup\Entity\MailGroup;
use Drupal\mailgroup\Entity\MailGroupMembership;
use Drupal\mailgroup\Entity\Storage\MailGroupMembershipStorageInterface;
use Psr\Log\LoggerInterface;

/**
 * Confirm form for bulk Mail Group Memberships create action.
 */
class MailGroupMembershipActionAddConfirm extends MailGroupMembershipActionConfirmBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'mailgroup_membership_multiple_add_form';
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    AccountInterface $current_user,
    LoggerInterface $logger,
    MailGroupMembershipStorageInterface $mailgroup_membership_storage,
    MessengerInterface $messenger,
    PrivateTempStoreFactory $temp_store_factory
  ) {
    parent::__construct($current_user, $logger, $mailgroup_membership_storage, $messenger, $temp_store_factory);
    $this->users = $this->tempStoreFactory->get('mailgroup_membership_multiple_add')->get($this->currentUser->id()) ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Create memberships');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->formatPlural(count($this->users),
      'Select the mail groups to create membership(s) for',
      'Select the mail groups to create membership(s) for');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $duplicates = 0;
    $success = 0;
    $uids = $this->getUids();

    $gids = $this->getSelectedMailGroups($form_state);
    foreach ($gids as $gid) {

      // First, remove Users with existing memberships for current group, as we
      // don't want to create duplicates.
      // For now, we don't care if they are active or not. Perhaps later, we can
      // add updating inactive memberships to active.
      $uids_with_membership = $this->mailGroupMembershipStorage->checkMembershipsByUids($uids, $gid);
      $uids_new_membership = array_diff($uids, $uids_with_membership);

      // Keep track of the amount we skip.
      $duplicates += count($uids_with_membership);

      foreach ($uids_new_membership as $uid) {

        $values = [
          'uid' => $uid,
          'gid' => $gid,
        ];

        try {
          MailGroupMembership::create($values)->save();
          $success++;
        }
        catch (\Exception $exception) {

          $params = [
            '@user' => $this->users[$uid],
            '@group' => MailGroup::load($gid)->label(),
          ];
          $this->messenger()->addError($this->t('Failed to create membership for user @user and group @group. See logs for details.', $params));

          $context = $values + ['error' => $exception->getMessage()];
          $this->logger->error('Failed to create membership for user {user} and group {group}: {error}', $context);
        }
      }
    }

    $this->tempStoreFactory->get('mailgroup_membership_multiple_add')->delete($this->currentUser->id());

    // No else, as either there should be duplicates or errors.
    if ($success > 0) {
      $this->messenger->addStatus($this->formatPlural($success, 'One membership created.', '@count memberships created.'));
    }

    if ($duplicates > 0) {
      $this->messenger->addWarning($this->formatPlural($duplicates, 'One user already has membership for a selected group and has been skipped.', '@count users already have memberships for a selected group and have been skipped.'));
    }

    parent::submitForm($form, $form_state);
  }

}
