<?php

namespace Drupal\mailgroup\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Drupal\mailgroup\Entity\MailGroup;
use Drupal\mailgroup\Entity\Storage\MailGroupMembershipStorageInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base for Mail Group Membership action Confirm form.
 */
abstract class MailGroupMembershipActionConfirmBase extends ConfirmFormBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The Mail Group Membership storage.
   *
   * @var \Drupal\mailgroup\Entity\Storage\MailGroupMembershipStorageInterface
   */
  protected $mailGroupMembershipStorage;

  /**
   * The Private Temp Store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * The User labels, keyed by their ID, to create memberships for.
   *
   * @var array
   */
  protected $users;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\mailgroup\Entity\Storage\MailGroupMembershipStorageInterface $mailgroup_membership_storage
   *   The Mail Group Membership storage.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The Private Temp Store factory.
   */
  public function __construct(
    AccountInterface $current_user,
    LoggerInterface $logger,
    MailGroupMembershipStorageInterface $mailgroup_membership_storage,
    MessengerInterface $messenger,
    PrivateTempStoreFactory $temp_store_factory
  ) {
    $this->currentUser = $current_user;
    $this->logger = $logger;
    $this->mailGroupMembershipStorage = $mailgroup_membership_storage;
    $this->messenger = $messenger;
    $this->tempStoreFactory = $temp_store_factory;
    $this->users = [];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('logger.channel.mailgroup'),
      $container->get('entity_type.manager')->getStorage('mailgroup_membership'),
      $container->get('messenger'),
      $container->get('tempstore.private')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $groups = MailGroup::loadMultiple();
    foreach ($groups as $id => $group) {
      $groups[$id] = $group->label();
    }

    $form['mail_groups'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Mail group(s)'),
      '#options' => $groups,
    ];

    $form['users'] = [
      '#theme' => 'item_list__mailgroup_users',
      '#title' => $this->t('User(s)'),
      '#items' => array_values($this->users),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.user.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

  /**
   * Extract the selected Mail Groups from the form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The selected Mail Groups.
   */
  protected function getSelectedMailGroups(FormStateInterface $form_state) {

    $gids = $form_state->getValue('mail_groups');

    // Remove the unchecked groups.
    $gids = array_flip($gids);
    unset($gids[0]);

    return $gids;
  }

  /**
   * Get the User Ids from the selected Users.
   *
   * @return array
   *   The User IDs.
   */
  protected function getUids() {
    return array_keys($this->users);
  }

}
