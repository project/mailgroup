<?php

namespace Drupal\mailgroup\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\mailgroup\Entity\MailGroup;
use Drupal\mailgroup\Entity\Storage\MailGroupMembershipStorageInterface;
use Psr\Log\LoggerInterface;

/**
 * Confirm form for bulk Mail Group Memberships deactivate action.
 */
class MailGroupMembershipActionDeactivateConfirm extends MailGroupMembershipActionConfirmBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'mailgroup_membership_multiple_activate_form';
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    AccountInterface $current_user,
    LoggerInterface $logger,
    MailGroupMembershipStorageInterface $mailgroup_membership_storage,
    MessengerInterface $messenger,
    PrivateTempStoreFactory $temp_store_factory
  ) {
    parent::__construct($current_user, $logger, $mailgroup_membership_storage, $messenger, $temp_store_factory);
    $this->users = $this->tempStoreFactory->get('mailgroup_membership_multiple_deactivate')->get($this->currentUser->id()) ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Deactivate memberships');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->formatPlural(count($this->users),
      'Select the mail groups to deactivate membership for',
      'Select the mail groups to deactivate memberships for');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $success = 0;
    $uids = $this->getUids();

    $gids = $this->getSelectedMailGroups($form_state);
    foreach ($gids as $gid) {

      $memberships = $this->mailGroupMembershipStorage->loadByUidsAndStatus($uids, TRUE, $gid);
      foreach ($memberships as $membership) {
        try {
          $membership->setInActive();
          $membership->save();
          $success++;
        }
        catch (\Exception $exception) {
          // Only show error message, no need to log. Storage parent function
          // already logs save errors.
          $params = [
            '@user' => $this->users[$membership->getOwnerId()],
            '@group' => MailGroup::load($gid)->label(),
          ];
          $this->messenger()->addError($this->t('Failed to deactivate membership for user @user and group @group. See logs for details.', $params));
        }
      }
    }

    $this->tempStoreFactory->get('mailgroup_membership_multiple_deactivate')->delete($this->currentUser->id());

    if ($success > 0) {
      $this->messenger->addStatus($this->formatPlural($success, 'One membership deactivated.', '@count memberships deactivated.'));
    }
    else {
      $this->messenger->addWarning($this->t('No memberships to deactivate for the selected group(s).'));
    }

    parent::submitForm($form, $form_state);
  }

}
