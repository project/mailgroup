<?php

namespace Drupal\mailgroup\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\mailgroup\Entity\MailGroup;
use Drupal\mailgroup\Entity\Storage\MailGroupMembershipStorageInterface;
use Psr\Log\LoggerInterface;

/**
 * Confirm form for bulk Mail Group Memberships delete action.
 */
class MailGroupMembershipActionDeleteConfirm extends MailGroupMembershipActionConfirmBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'mailgroup_membership_multiple_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    AccountInterface $current_user,
    LoggerInterface $logger,
    MailGroupMembershipStorageInterface $mailgroup_membership_storage,
    MessengerInterface $messenger,
    PrivateTempStoreFactory $temp_store_factory
  ) {
    parent::__construct($current_user, $logger, $mailgroup_membership_storage, $messenger, $temp_store_factory);
    $this->users = $this->tempStoreFactory->get('mailgroup_membership_multiple_delete')->get($this->currentUser->id()) ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Delete memberships');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->formatPlural(count($this->users),
      'Select the mail groups to delete membership for',
      'Select the mail groups to delete memberships for');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $success = 0;
    $uids = $this->getUids();

    $gids = $this->getSelectedMailGroups($form_state);
    foreach ($gids as $gid) {

      try {
        $success += $this->mailGroupMembershipStorage->deleteByUids($uids, $gid);
      }
      catch (\Exception $exception) {
        // Only show error message, no need to log. Storage parent function
        // already logs delete errors.
        $params = [
          '@group' => MailGroup::load($gid)->label(),
        ];
        $this->messenger()->addError($this->t('Failed to delete membership(s) for group @group. See logs for details.', $params));
      }
    }

    $this->tempStoreFactory->get('mailgroup_membership_multiple_delete')->delete($this->currentUser->id());
    $this->messenger->addStatus($this->formatPlural($success, 'One membership deleted.', '@count memberships deleted.'));

    parent::submitForm($form, $form_state);
  }

}
