<?php

namespace Drupal\mailgroup;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin manager for mail group connections.
 */
class ConnectionManager extends DefaultPluginManager {

  /**
   * Constructs an ConnectionManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Mailgroup/Connection',
      $namespaces,
      $module_handler,
      'Drupal\mailgroup\ConnectionInterface',
      'Drupal\mailgroup\Annotation\Connection',
    );

    $this->alterInfo('mailgroup_connection');
    $this->setCacheBackend($cache_backend, 'mailgroup_connection_plugins');
  }

  /**
   * Get configuration fields to store encrypted for Connection plugin.
   *
   * @param string $plugin_id
   *   The plugin ID.
   *
   * @return array
   *   An array of field IDs.
   */
  public function getPluginFieldsToEncrypt(string $plugin_id) {

    $fields_to_encrypt = [];

    if ($this->hasDefinition($plugin_id)) {
      $definition = $this->getDefinition($plugin_id);
      $fields_to_encrypt = $definition['fields_to_encrypt'] ?? [];
    }

    return $fields_to_encrypt;
  }

}
