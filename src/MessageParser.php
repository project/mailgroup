<?php

namespace Drupal\mailgroup;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\mailgroup\Entity\MailGroupInterface;
use ZBateson\MailMimeParser\Header\HeaderConsts;
use ZBateson\MailMimeParser\IMessage;

class MessageParser implements MessageParserInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The group this message belongs to.
   *
   * @var \Drupal\mailgroup\Entity\MailGroupInterface
   */
  protected $group;

  /**
   * The Message object.
   *
   * @var \ZBateson\MailMimeParser\IMessage
   */
  protected $message;

  /**
   * Set the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @return $this
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    return $this;
  }

  /**
   * Set the group entity.
   *
   * @param \Drupal\mailgroup\Entity\MailGroupInterface $group
   *   The group entity.
   *
   * @return $this
   */
  public function setGroup(MailGroupInterface $group) {
    $this->group = $group;
    return $this;
  }

  /**
   * Set the message object.
   *
   * @param \ZBateson\MailMimeParser\IMessage $message
   *   The message object.
   *
   * @return $this
   */
  public function setMessage(IMessage $message) {
    $this->message = $message;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function createMessageEntity() {
    $message_storage = $this->entityTypeManager->getStorage('mailgroup_message');

    $from_email = $this->message->getHeaderValue(HeaderConsts::FROM);
    $subject = $this->message->getHeaderValue(HeaderConsts::SUBJECT);

    $sending_member = $this->group->isAllowedToSend($from_email);

    $values = [
      'gid' => $this->group->id(),
      'uid' => $sending_member->getOwnerId(),
      'subject' => $subject,
      'body' => $this->message->getTextContent(),
    ];

    /** @var \Drupal\mailgroup\Entity\MailGroupMessageInterface $message */
    $message = $message_storage->create($values);
    return $message;
  }

}
