<?php

namespace Drupal\mailgroup\Plugin\views\field;

use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\mailgroup\Entity\Storage\MailGroupMembershipStorageInterface;
use Drupal\views\Plugin\views\field\PrerenderList;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Mail Group Memberships of User.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("mailgroup_memberships")
 */
class MailGroupMemberships extends PrerenderList {

  /**
   * The current User.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The Mail Group Membership storage.
   *
   * @var \Drupal\mailgroup\Entity\Storage\MailGroupMembershipStorageInterface
   */
  protected $mailGroupMembershipStorage;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current User.
   * @param \Drupal\mailgroup\Entity\Storage\MailGroupMembershipStorageInterface $mailgroup_membership_storage
   *   The Mail Group Membership storage.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    AccountProxyInterface $account,
    MailGroupMembershipStorageInterface $mailgroup_membership_storage,
    RendererInterface $renderer
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->currentUser = $account;
    $this->mailGroupMembershipStorage = $mailgroup_membership_storage;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('mailgroup_membership'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function preRender(&$values) {

    $uids = [];
    foreach ($values as $result) {
      $uids[] = $result->{$this->field_alias};
    }

    $memberships = $this->mailGroupMembershipStorage->loadByUids($uids, NULL, FALSE);
    foreach ($memberships as $membership) {

      $build = [
        '#theme' => 'mailgroup_membership_list_item',
        '#label' => $membership->getGroup()->label(),
        '#status' => $membership->isActive(),
        '#url' => $membership->toUrl('collection')->toString(),
      ];

      $this->items[$membership->getOwnerId()][]['mailgroup_membership'] = $this->renderer->render($build);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render_item($count, $item) {
    return $item['mailgroup_membership'];
  }

}
