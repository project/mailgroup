<?php

namespace Drupal\mailgroup\Plugin\views\filter;

use Drupal\Core\Database\Database;
use Drupal\mailgroup\Entity\MailGroup;
use Drupal\views\Plugin\views\filter\InOperator;

/**
 * Mail Group Memberships filter by Mail Group for Users.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("mailgroup_memberships")
 */
class MailGroupMemberships extends InOperator {

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {

    $mailgroups = MailGroup::loadMultiple();
    foreach ($mailgroups as $id => $mailgroup) {
      $this->valueOptions[$id] = $mailgroup->label();
    }

    return $this->valueOptions;
  }

  /**
   * {@inheritdoc}
   */
  protected function opSimple() {
    if (empty($this->value)) {
      return;
    }
    $this->ensureMyTable();

    // Using a sub query avoids having to work with table joins or views
    // relationship definitions.
    $subselect = Database::getConnection()->select('mailgroup_membership', 'mm');
    $subselect->addField('mm', 'uid');
    $subselect->condition('mm.gid', $this->value, 'IN');

    $this->query->addWhere($this->options['group'], "$this->tableAlias.$this->realField", $subselect, $this->operator);
  }

}
