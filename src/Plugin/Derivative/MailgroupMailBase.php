<?php

namespace Drupal\mailgroup\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides deriver base for the Mail Group Mail plugins.
 */
abstract class MailgroupMailBase extends DeriverBase implements MailgroupMailInterface {

  /**
   * The Mail Group storage.
   *
   * @var \Drupal\mailgroup\Entity\Storage\MailGroupStorageInterface
   */
  protected $mailGroupStorage;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->mailGroupStorage = $entity_type_manager->getStorage('mailgroup');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {

    $connection_plugin_ids = $this->getConnectionPluginIds();
    foreach ($connection_plugin_ids as $connection_plugin_id) {

      $mailgroup_ids = $this->mailGroupStorage->getIdsByConnectionPlugin($connection_plugin_id);
      foreach ($mailgroup_ids as $mailgroup_id) {
        $this->derivatives[$mailgroup_id] = $base_plugin_definition;
      }
    }

    return $this->derivatives;
  }

}
