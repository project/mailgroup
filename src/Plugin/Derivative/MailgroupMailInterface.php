<?php

namespace Drupal\mailgroup\Plugin\Derivative;

use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;

/**
 * Defines a deriver for Mail plugins.
 */
interface MailgroupMailInterface extends ContainerDeriverInterface {

  /**
   * Get the IDs of the Connection plugins that support outgoing mail.
   *
   * @return array
   *   List of Connection plugin IDs.
   */
  public function getConnectionPluginIds();

}
