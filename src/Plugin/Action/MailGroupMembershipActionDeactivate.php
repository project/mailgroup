<?php

namespace Drupal\mailgroup\Plugin\Action;

use Drupal\Core\Session\AccountInterface;

/**
 * Deactivate Mail Group Memberships for selected Users.
 *
 * @Action(
 *   id = "mailgroup_membership_multiple_deactivate",
 *   label = @Translation("Deactivate mail group membership(s) for the selected user(s)"),
 *   type= "user",
 *   confirm_form_route_name = "mailgroup_membership.multiple.deactivate"
 * )
 */
class MailGroupMembershipActionDeactivate extends MailGroupMembershipActionBase {

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $objects) {
    $this->setTempStore('mailgroup_membership_multiple_deactivate', $objects);
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = $this->hasPermission('edit mail group memberships', $account);
    return $return_as_object ? $result : $result->isAllowed();
  }

}
