<?php

namespace Drupal\mailgroup\Plugin\Action;

use Drupal\Core\Session\AccountInterface;

/**
 * Create Mail Group Memberships for selected Users.
 *
 * @Action(
 *   id = "mailgroup_membership_multiple_add",
 *   label = @Translation("Add mail group membership(s) for the selected user(s)"),
 *   type= "user",
 *   confirm_form_route_name = "mailgroup_membership.multiple.add"
 * )
 */
class MailGroupMembershipActionAdd extends MailGroupMembershipActionBase {

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $objects) {
    $this->setTempStore('mailgroup_membership_multiple_add', $objects);
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = $this->hasPermission('add mail group memberships', $account);
    return $return_as_object ? $result : $result->isAllowed();
  }

}
