<?php

namespace Drupal\mailgroup\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Action\ActionBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base for Mail Group Memberships actions.
 */
abstract class MailGroupMembershipActionBase extends ActionBase implements ContainerFactoryPluginInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The Mail Group Membership entity type definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityType;

  /**
   * The Private Temp Store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The Private Temp Store factory.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    AccountInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    PrivateTempStoreFactory $temp_store_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->currentUser = $current_user;
    $this->entityType = $entity_type_manager->getDefinition('mailgroup_membership');
    $this->tempStoreFactory = $temp_store_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('tempstore.private')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute($object = NULL) {
    $this->executeMultiple([$object]);
  }

  /**
   * Store the User IDs from the selected Users.
   *
   * @param string $collection
   *   The collection name to use for the temp store.
   * @param array $objects
   *   An array of Users.
   */
  protected function setTempStore(string $collection, array $objects) {
    $uids = [];

    /** @var \Drupal\user\UserInterface $user */
    foreach ($objects as $user) {
      $uids[$user->id()] = $user->label();
    }

    $this->tempStoreFactory->get($collection)->set($this->currentUser->id(), $uids);
  }

  /**
   * Check if the User has the proper permission for Mail Group Membership.
   *
   * @param string $action_permission
   *   The permission string corresponding with the action.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   The User for which to check access for. Defaults NULL, in which case
   *   the current User will be used.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The Access Result.
   */
  protected function hasPermission(string $action_permission, AccountInterface $account = NULL) {

    if ($account === NULL) {
      $account = $this->currentUser;
    }

    $admin_permission = $this->entityType->getAdminPermission();
    if ($account->hasPermission($admin_permission)) {
      $result = AccessResult::allowed();
    }
    else {
      $result = AccessResult::allowedIfHasPermission($account, $action_permission);
    }

    return $result;
  }

}
