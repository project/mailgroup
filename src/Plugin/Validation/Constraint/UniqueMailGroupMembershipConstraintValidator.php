<?php

namespace Drupal\mailgroup\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Constraint validator for unique MailGroup Memberships.
 */
class UniqueMailGroupMembershipConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\mailgroup\Entity\Storage\MailGroupMembershipStorageInterface
   */
  protected $storage;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->storage = $entity_type_manager->getStorage('mailgroup_membership');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint) {

    /** @var \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem $field_item */
    $field_item = $entity->first();

    $membership = $field_item->getParent()->getParent()->getEntity();
    $uids = $this->storage->checkMembershipsByUids([$field_item->getString()], $membership->getGroupId());

    if (!empty($uids)) {
      $this->context->buildViolation($constraint->message)->addViolation();
    }
  }

}
