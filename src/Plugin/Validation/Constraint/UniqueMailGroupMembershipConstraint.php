<?php

namespace Drupal\mailgroup\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Validation constraint MailGroup Memberships.
 *
 * @Constraint(
 *   id = "UniqueMailGroupMembership",
 *   label = @Translation("MailGroup Membership.", context = "Validation"),
 * )
 */
class UniqueMailGroupMembershipConstraint extends Constraint {

  /**
   * The violation message.
   *
   * @var string
   */
  public $message = 'The user is already a member of this group.';

}
