<?php

namespace Drupal\mailgroup\Config;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\StorageInterface;
use Drupal\mailgroup\MailgroupMailDerivativeTrait;

/**
 * Base class for Mail plugin derivatives for Connection plugins.
 */
abstract class MailPluginOverrideBase implements MailPluginOverrideInterface {

  use MailgroupMailDerivativeTrait;

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name) {
    return (new CacheableMetadata())->setCacheTags(['mailgroup_list']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix() {
    return "{$this->getModule()}_mail_plugins";
  }

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    $overrides = [];

    // We need to add our derivative Mail plugin IDs to the
    // system.mail.interface list.
    $name = 'system.mail';
    if (in_array($name, $names)) {

      $storage = \Drupal::entityTypeManager()->getStorage('mailgroup');
      $connection_plugin_ids = $this->getConnectionPluginIds();
      foreach ($connection_plugin_ids as $connection_plugin_id) {

        $mailgroup_ids = $storage->getIdsByConnectionPlugin($connection_plugin_id);
        foreach ($mailgroup_ids as $mailgroup_id) {

          $id = $this->buildMailPluginDerivativeId($connection_plugin_id, $mailgroup_id);
          $overrides[$name]['interface'][$id] = $id;
        }
      }
    }

    return $overrides;
  }

}
