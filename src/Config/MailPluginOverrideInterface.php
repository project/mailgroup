<?php

namespace Drupal\mailgroup\Config;

use Drupal\Core\Config\ConfigFactoryOverrideInterface;

/**
 * Defines config override generator to support derivative Mail plugins.
 */
interface MailPluginOverrideInterface extends ConfigFactoryOverrideInterface {

  /**
   * Get the IDs of the Connection plugins that support outgoing mail.
   *
   * @return array
   *   List of Connection plugin IDs.
   */
  public function getConnectionPluginIds();

  /**
   * Get the module system name.
   *
   * @return string
   *   The module system name.
   */
  public function getModule();

}
