<?php

namespace Drupal\mailgroup\Entity\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for mail group membership entities.
 */
class MailGroupMembershipRouteProvider extends MailGroupEntityRouteProvider {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    $entity_type_id = $entity_type->id();

    if ($activate_route = $this->getActivateRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.activate_form", $activate_route);
    }

    if ($deactivate_route = $this->getDeactivateRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.deactivate_form", $deactivate_route);
    }

    return $collection;
  }

  /**
   * Gets the membership activation route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getActivateRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('activate-form')) {
      $route = new Route($entity_type->getLinkTemplate('activate-form'));

      $route
        ->setDefaults([
          '_entity_form' => 'mailgroup_membership.activate',
          '_title' => $this->t('Activate Membership'),
        ])
        ->setRequirements([
          '_entity_access' => 'mailgroup_membership.update',
          'mailgroup_membership' => '\d+',
        ])
        ->setOption('parameters', [
          'mailgroup' => ['type' => 'entity:mailgroup'],
          'mailgroup_membership' => ['type' => 'entity:mailgroup_membership'],
        ]);

      return $route;
    }
  }

  /**
   * Gets the membership deactivation route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getDeactivateRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('deactivate-form')) {
      $route = new Route($entity_type->getLinkTemplate('deactivate-form'));

      $route
        ->setDefaults([
          '_entity_form' => 'mailgroup_membership.deactivate',
          '_title' => $this->t('Deactivate Membership'),
        ])
        ->setRequirements([
          '_entity_access' => 'mailgroup_membership.update',
          'mailgroup_membership' => '\d+',
        ])
        ->setOption('parameters', [
          'mailgroup' => ['type' => 'entity:mailgroup'],
          'mailgroup_membership' => ['type' => 'entity:mailgroup_membership'],
        ]);

      return $route;
    }
  }

}
