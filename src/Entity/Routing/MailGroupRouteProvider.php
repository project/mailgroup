<?php

namespace Drupal\mailgroup\Entity\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides routes for mail groups.
 */
class MailGroupRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getAddPageRoute(EntityTypeInterface $entity_type) {
    $route = parent::getAddPageRoute($entity_type);
    $route->setDefault('_controller', 'Drupal\mailgroup\Controller\MailGroupController::addPage');

    return $route;
  }

}
