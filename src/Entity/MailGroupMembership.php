<?php

namespace Drupal\mailgroup\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the mail group membership entity.
 *
 * @ContentEntityType(
 *   id = "mailgroup_membership",
 *   label = @Translation("Membership"),
 *   label_collection = @Translation("Memberships"),
 *   label_singular = @Translation("membership"),
 *   label_plural = @Translation("memberships"),
 *   label_count = @PluralTranslation(
 *     singular = "@count membership",
 *     plural = "@count memberships"
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\mailgroup\Entity\Storage\MailGroupMembershipStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\mailgroup\Entity\ListBuilder\MailGroupMembershipListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\mailgroup\Entity\Form\MailGroupMembershipForm",
 *       "add" = "Drupal\mailgroup\Entity\Form\MailGroupMembershipForm",
 *       "edit" = "Drupal\mailgroup\Entity\Form\MailGroupMembershipForm",
 *       "activate" = "Drupal\mailgroup\Entity\Form\MailGroupMembershipActivateForm",
 *       "deactivate" = "Drupal\mailgroup\Entity\Form\MailGroupMembershipDeactivateForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "access" = "Drupal\mailgroup\Entity\Access\MailGroupMembershipAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\mailgroup\Entity\Routing\MailGroupMembershipRouteProvider",
 *     },
 *   },
 *   base_table = "mailgroup_membership",
 *   admin_permission = "administer mailgroups",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *     "gid" = "gid",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "add-form" = "/mailgroup/{mailgroup}/members/add",
 *     "edit-form" = "/mailgroup/{mailgroup}/members/{mailgroup_membership}/edit",
 *     "activate-form" = "/mailgroup/{mailgroup}/members/{mailgroup_membership}/activate",
 *     "deactivate-form" = "/mailgroup/{mailgroup}/members/{mailgroup_membership}/deactivate",
 *     "delete-form" = "/mailgroup/{mailgroup}/members/{mailgroup_membership}/delete",
 *     "collection" = "/mailgroup/{mailgroup}/members",
 *   },
 * )
 */
class MailGroupMembership extends MailGroupEntityBase implements MailGroupMembershipInterface {

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setActive() {
    $this->set('status', TRUE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setInactive() {
    $this->set('status', FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['gid']->setDescription(t('The group to add the member to.'));

    $fields['uid']
      ->setLabel(t('User'))
      ->setDescription(t('The user ID of the group member.'))
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->addConstraint('UniqueMailGroupMembership');

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Active'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
