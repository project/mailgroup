<?php

namespace Drupal\mailgroup\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\mailgroup\Exception\MailGroupInactiveException;
use Drupal\mailgroup\Exception\MemberNotFoundException;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the Mail group entity.
 *
 * @ingroup mailgroup
 *
 * @ContentEntityType(
 *   id = "mailgroup",
 *   label = @Translation("Mail group"),
 *   label_collection = @Translation("Mail groups"),
 *   label_singular = @Translation("group"),
 *   label_plural = @Translation("groups"),
 *   label_count = @PluralTranslation(
 *     singular = "@count group",
 *     plural = "@count groups"
 *   ),
 *   bundle_label = @Translation("mail group type"),
 *   handlers = {
 *     "storage" = "Drupal\mailgroup\Entity\Storage\MailGroupStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\mailgroup\Entity\ListBuilder\MailGroupListBuilder",
 *     "views_data" = "Drupal\mailgroup\Entity\Views\MailGroupViewsData",
 *     "form" = {
 *       "default" = "Drupal\mailgroup\Entity\Form\MailGroupForm",
 *       "add" = "Drupal\mailgroup\Entity\Form\MailGroupForm",
 *       "edit" = "Drupal\mailgroup\Entity\Form\MailGroupForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "access" = "Drupal\mailgroup\Entity\Access\MailGroupAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\mailgroup\Entity\Routing\MailGroupRouteProvider",
 *     },
 *   },
 *   base_table = "mailgroup",
 *   admin_permission = "administer mail groups",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/mailgroup/{mailgroup}",
 *     "add-page" = "/admin/mailgroup/add",
 *     "add-form" = "/admin/mailgroup/add/{mailgroup_type}",
 *     "edit-form" = "/mailgroup/{mailgroup}/edit",
 *     "delete-form" = "/mailgroup/{mailgroup}/delete",
 *     "collection" = "/admin/mailgroup",
 *   },
 *   bundle_entity_type = "mailgroup_type",
 *   field_ui_base_route = "entity.mailgroup_type.edit_form"
 * )
 */
class MailGroup extends ContentEntityBase implements MailGroupInterface {
  use EntityOwnerTrait;
  use StringTranslationTrait;

  /**
   * The connection plugin manager.
   *
   * @var \Drupal\mailgroup\ConnectionManager
   */
  protected $connectionManager;

  /**
   * The Encryption Profile.
   *
   * @var \Drupal\encrypt\EncryptionProfileInterface
   */
  protected $encryptionProfile;

  /**
   * The Encryption service.
   *
   * @var \Drupal\encrypt\EncryptServiceInterface
   */
  protected $encryptionService;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmail() {
    return $this->get('email')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEmail($email) {
    $this->set('email', $email);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function prependName() {
    return (bool) $this->get('prepend_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPrependName($prepend) {
    $this->set('prepend_name', $prepend);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getReplyTo() {
    return $this->get('reply_to')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setReplyTo($email) {
    $this->set('reply_to', $email);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSendType() {
    return $this->get('send_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSendType($send_type) {
    $this->set('send_type', $send_type);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConnectionPlugin() {
    $plugin_id = $this->getConnectionPluginId();

    if ($plugin_id) {
      $plugin = $this->getConnectionManager()->createInstance($plugin_id, $this->getConnectionConfig());
    }
    else {
      $plugin = NULL;
    }

    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function setConnectionPlugin($plugin) {
    $this->set('plugin', $plugin);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConnectionPluginId() {
    return $this->get('connection')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getConnectionConfig() {

    $config = $this->get('connection_config')->first();
    $config = $config ? $config->getValue() : [];

    $encryption_profile = $this->getEncryptionProfile();
    $fields_to_encrypt = $this->getPluginFieldsToEncrypt();

    if ($encryption_profile !== NULL && !empty($fields_to_encrypt)) {

      /** @var \Drupal\Core\Field\Plugin\Field\FieldType\StringLongItem $config_encrypted */
      $config_encrypted = $this->get('connection_config_encrypted')->first();
      if ($config_encrypted !== NULL && !$config_encrypted->isEmpty()) {

        $config_decrypted = $this->getEncryptionService()->decrypt($config_encrypted->value, $encryption_profile);
        $config_decrypted = unserialize($config_decrypted);

        $config = array_merge($config, $config_decrypted);
      }
    }

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function setConnectionConfig(array $config) {

    $encryption_profile = $this->getEncryptionProfile();
    $fields_to_encrypt = $this->getPluginFieldsToEncrypt();

    if ($encryption_profile !== NULL && !empty($fields_to_encrypt)) {

      $config_encrypt = [];
      foreach ($fields_to_encrypt as $field) {

        $config_encrypt[$field] = $config[$field];
        unset($config[$field]);
      }

      $config_encrypt = serialize($config_encrypt);
      $config_encrypted = $this->getEncryptionService()->encrypt($config_encrypt, $encryption_profile);

      $this->set('connection_config_encrypted', $config_encrypted);
    }

    return $this->set('connection_config', $config);
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setActive() {
    $this->set('status', TRUE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setInactive() {
    $this->set('status', FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function testConnection() {
    /** @var \Drupal\mailgroup\ConnectionInterface $plugin */
    $plugin = $this->getConnectionPlugin();
    $status = $plugin->testConnection();

    return $status;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessages() {
    $storage = $this->entityTypeManager()->getStorage('mailgroup_message');

    /** @var \Drupal\mailgroup\Entity\MailGroupMessageInterface[] $messages */
    $messages = $storage->loadByProperties([
      'gid' => $this->id(),
    ]);

    return $messages;
  }

  /**
   * {@inheritdoc}
   */
  public function getMember($email) {
    $members = $this->getMembershipStorage()->loadByEmail($email, $this);
    return empty($members) ? NULL : reset($members);
  }

  /**
   * {@inheritdoc}
   */
  public function getMembers() {
    return $this->getMembershipStorage()->loadByGroup($this);
  }

  /**
   * {@inheritdoc}
   */
  public function getMemberEmails() {
    $emails = [];
    $members = $this->getMembers();

    foreach ($members as $member) {
      $emails[] = $member->getOwner()->getEmail();
    }

    return $emails;
  }

  /**
   * {@inheritdoc}
   */
  public function isMember($email) {
    $member = $this->getMember($email);
    return $member instanceof MailGroupMembershipInterface;
  }

  /**
   * {@inheritdoc}
   */
  public function isAllowedToSend($email) {

    if (!$this->isActive()) {
      $message = $this->t('The group @group is not active.', [
        '@group' => $this->getName(),
      ]);
      throw new MailGroupInactiveException($message);
    }

    $member = $this->getMember($email);
    if (!$member instanceof MailGroupMembershipInterface) {
      $message = $this->t('@email is not an (active) member of group @group.', [
        '@group' => $this->getName(),
        '@email' => $email,
      ]);
      throw new MemberNotFoundException($message);
    }

    return $member;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Name'))
      ->setDescription(new TranslatableMarkup('The name of the group.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid']
      ->setLabel(new TranslatableMarkup('Created by'))
      ->setDescription(new TranslatableMarkup('The username of the group creator.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 52,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['email'] = BaseFieldDefinition::create('email')
      ->setLabel(new TranslatableMarkup('Email'))
      ->setDescription(new TranslatableMarkup('The email address of the group.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['prepend_name'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Prepend list name to the subject?'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['reply_to'] = BaseFieldDefinition::create('list_integer')
      ->setLabel(new TranslatableMarkup('Reply to'))
      ->setDescription(new TranslatableMarkup('Who should replies to group messages be sent to?'))
      ->setRequired(TRUE)
      ->setSettings([
        'allowed_values' => [
          MailGroupInterface::REPLY_ALL => new TranslatableMarkup('All members'),
          MailGroupInterface::REPLY_SENDER => new TranslatableMarkup('Sender only'),
        ],
      ])
      ->setPropertyConstraints('value', [
        'AllowedValues' => [
          MailGroupInterface::REPLY_ALL,
          MailGroupInterface::REPLY_SENDER,
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['send_type'] = BaseFieldDefinition::create('list_integer')
      ->setLabel(new TranslatableMarkup('Send type'))
      ->setDescription(new TranslatableMarkup('Should messages be sent to members individually, or
        as a single message with all members added to the BCC?'))
      ->setRequired(TRUE)
      ->setSettings([
        'allowed_values' => [
          MailGroupInterface::SEND_INDIVIDUAL => new TranslatableMarkup('Individual'),
          MailGroupInterface::SEND_GROUPED => new TranslatableMarkup('Grouped'),
        ],
      ])
      ->setPropertyConstraints('value', [
        'AllowedValues' => [
          MailGroupInterface::SEND_INDIVIDUAL,
          MailGroupInterface::SEND_GROUPED,
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $connection_manager = \Drupal::service('plugin.manager.mailgroup_connection');
    $plugins = $connection_manager->getDefinitions();
    $plugin_options = [];
    foreach ($plugins as $plugin) {
      $plugin_options[$plugin['id']] = $plugin['label'];
    }

    $fields['connection'] = BaseFieldDefinition::create('list_string')
      ->setLabel(new TranslatableMarkup('Connection plugin'))
      ->setDescription(new TranslatableMarkup('The connection plugin to use to receive mail.'))
      ->setSettings([
        'allowed_values' => $plugin_options,
      ])
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 51,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['connection_config'] = BaseFieldDefinition::create('map')
      ->setLabel(new TranslatableMarkup('Connection configuration'))
      ->setDescription(new TranslatableMarkup('The configuration for the connection plugin.'));

    $fields['connection_config_encrypted'] = BaseFieldDefinition::create('string_long')
      ->setSetting('case_sensitive', TRUE)
      ->setLabel(new TranslatableMarkup('Connection configuration, encrypted.'))
      ->setDescription(new TranslatableMarkup('The encrypted configuration for the connection plugin.'));

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Active'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Get the connection plugin manager.
   *
   * @return \Drupal\mailgroup\ConnectionManager
   *   The connection plugin manager.
   */
  protected function getConnectionManager() {

    if (!isset($this->connectionManager)) {
      $this->connectionManager = \Drupal::service('plugin.manager.mailgroup_connection');
    }

    return $this->connectionManager;
  }

  /**
   * Get the Encryption Profile.
   *
   * @return \Drupal\encrypt\EncryptionProfileInterface|null
   *   The Encryption Profile.
   */
  protected function getEncryptionProfile() {

    if (!isset($this->encryptionProfile)) {
      $type = MailGroupType::load($this->bundle());

      // Currently, Mail Group Types can be deleted while there are still Mail
      // Groups using it. Until that is fixed, we cannot be sure the type is
      // always present.
      if ($type !== NULL) {
        $this->encryptionProfile = $type->getEncryptionProfile();
      }
    }

    return $this->encryptionProfile;
  }

  /**
   * Get the Encryption service.
   *
   * @return \Drupal\encrypt\EncryptServiceInterface
   *   The Encryption service.
   */
  protected function getEncryptionService() {

    if (!isset($this->encryptionService)) {
      $this->encryptionService = \Drupal::service('encryption');
    }

    return $this->encryptionService;
  }

  /**
   * Get the Mail Group Membership storage.
   *
   * @return \Drupal\mailgroup\Entity\Storage\MailGroupMembershipStorageInterface
   *   The Membership storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getMembershipStorage() {

    /** @var \Drupal\mailgroup\Entity\Storage\MailGroupMembershipStorageInterface $storage */
    $storage = $this->entityTypeManager()->getStorage('mailgroup_membership');
    return $storage;
  }

  /**
   * Get Connection plugin configuration fields to store encrypted.
   *
   * @return array
   *   An array of field IDs.
   */
  protected function getPluginFieldsToEncrypt() {

    $fields_to_encrypt = [];

    $plugin_id = $this->getConnectionPluginId();
    if ($plugin_id) {

      // Use the plugin manager to get the plugin definition, as using
      // $this->getConnectionPlugin() would create infinite loop.
      $fields_to_encrypt = $this->getConnectionManager()
        ->getPluginFieldsToEncrypt($plugin_id);
    }

    return $fields_to_encrypt;
  }

}
