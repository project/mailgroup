<?php

namespace Drupal\mailgroup\Entity;

/**
 * Provides an interface for defining mail group memberships.
 */
interface MailGroupMembershipInterface extends MailGroupEntityInterface {

  /**
   * Returns the membership status.
   *
   * @return bool
   *   TRUE if the membership is active.
   */
  public function isActive();

  /**
   * Sets the membership status to active.
   *
   * @return $this
   */
  public function setActive();

  /**
   * Sets the membership status to inactive.
   *
   * @return $this
   */
  public function setInactive();

}
