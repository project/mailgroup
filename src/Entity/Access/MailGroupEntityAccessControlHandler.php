<?php

namespace Drupal\mailgroup\Entity\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for mail group entities.
 */
class MailGroupEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\mailgroup\Entity\MailGroupEntityInterface $entity */

    switch ($operation) {
      case 'view':
      case 'update':
      case 'delete':
        $admin_permission = $this->entityType->getAdminPermission();
        return AccessResult::allowedIfHasPermission($account, $admin_permission);
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    $admin_permission = $this->entityType->getAdminPermission();
    return AccessResult::allowedIfHasPermission($account, $admin_permission);
  }

}
