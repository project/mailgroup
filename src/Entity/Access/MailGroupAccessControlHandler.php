<?php

namespace Drupal\mailgroup\Entity\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for mail groups.
 */
class MailGroupAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\mailgroup\Entity\MailGroupInterface $entity */

    if ($account->hasPermission('administer mail groups')) {
      return AccessResult::allowed();
    }

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view mail groups');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit mail groups');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete mail groups');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {

    $admin_permission = $this->entityType->getAdminPermission();
    if ($account->hasPermission($admin_permission)) {
      return AccessResult::allowed();
    }

    return AccessResult::allowedIfHasPermission($account, 'add mail groups');
  }

}
