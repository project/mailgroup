<?php

namespace Drupal\mailgroup\Entity\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for Mail Group Memberships.
 */
class MailGroupMembershipAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    $admin_permission = $this->entityType->getAdminPermission();
    if ($account->hasPermission($admin_permission)) {
      return AccessResult::allowed();
    }

    switch ($operation) {
      case 'view':

        /** @var \Drupal\mailgroup\Entity\MailGroupInterface $entity */
        if ($entity->getOwnerId() === $account->id()) {
          $owner_view_access = AccessResult::allowedIfHasPermission($account, 'view own mail group memberships');
        }
        else {
          $owner_view_access = AccessResult::neutral();
        }

        return AccessResult::allowedIfHasPermission($account, 'view mail group memberships')->orIf($owner_view_access);

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit mail group memberships');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete mail group memberships');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {

    $admin_permission = $this->entityType->getAdminPermission();
    if ($account->hasPermission($admin_permission)) {
      return AccessResult::allowed();
    }

    return AccessResult::allowedIfHasPermission($account, 'add mail group memberships');
  }

}
