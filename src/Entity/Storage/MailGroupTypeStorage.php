<?php

namespace Drupal\mailgroup\Entity\Storage;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

/**
 * Storage handler for Mail Group Type entities.
 */
class MailGroupTypeStorage extends ConfigEntityStorage implements MailGroupTypeStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadWithMissingEncryption() {

    $types = $this->loadMultipleOverrideFree();
    foreach ($types as $id => $type) {

      if ($type->getEncryptionProfile() !== NULL) {
        unset($types[$id]);
      }
    }

    return $types;
  }

}
