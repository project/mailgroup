<?php

namespace Drupal\mailgroup\Entity\Storage;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\mailgroup\Entity\MailGroupInterface;

/**
 * Storage handler for Membership entities.
 */
interface MailGroupMembershipStorageInterface extends ContentEntityStorageInterface {

  /**
   * Returns the User IDs that have a membership.
   *
   * This function has no access checks and is intended for usage in bulk
   * actions only.
   *
   * @param array $uids
   *   The User IDs to check against.
   * @param int|null $gid
   *   The Mail Group ID to check against. Defaults to NULL, in which case all
   *   Mail Groups are taken into account.
   *
   * @return array
   *   List of User IDs, with duplicates already removed.
   */
  public function checkMembershipsByUids(array $uids, int $gid = NULL);

  /**
   * Deletes memberships for given User IDs.
   *
   * This function has no access checks and is intended for usage in bulk
   * actions only.
   *
   * @param array $uids
   *   The User IDs whose memberships to delete.
   * @param int|null $gid
   *   The Mail Group ID to filter on. Defaults to NULL, in which case all
   *   Mail Groups are taken into account.
   *
   * @return int
   *   The amount of deleted memberships.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteByUids(array $uids, int $gid = NULL);

  /**
   * Load Mail Group Memberships by email address.
   *
   * @param string $email
   *   The membership email address.
   * @param \Drupal\mailgroup\Entity\MailGroupInterface|null $group
   *   The Mail Group to filter on. Defaults to NULL, in which case all Mail
   *   Groups are taken into account.
   * @param bool $active
   *   Whether to return active memberships only. Defaults to TRUE.
   *
   * @return \Drupal\mailgroup\Entity\MailGroupMembershipInterface[]
   *   An array of memberships indexed by their ids.
   */
  public function loadByEmail(string $email, MailGroupInterface $group = NULL, bool $active = TRUE);

  /**
   * Load Memberships by Mail Group.
   *
   * @param \Drupal\mailgroup\Entity\MailGroupInterface $group
   *   The Mail Group to load the membership for.
   * @param bool $active
   *   Whether to return active memberships only. Defaults to TRUE.
   *
   * @return \Drupal\mailgroup\Entity\MailGroupMembershipInterface[]
   *   An array of memberships indexed by their ids.
   */
  public function loadByGroup(MailGroupInterface $group, bool $active = TRUE);

  /**
   * Load Memberships by User ID.
   *
   * @param int $uid
   *   The User ID.
   * @param \Drupal\mailgroup\Entity\MailGroupInterface|null $group
   *   The Mail Group to filter on. Defaults to NULL, in which case all Mail
   *   Groups are taken into account.
   * @param bool $active
   *   Whether to return active memberships only. Defaults to TRUE.
   *
   * @return \Drupal\mailgroup\Entity\MailGroupMembershipInterface[]
   *   An array of memberships indexed by their ids.
   */
  public function loadByUid(int $uid, MailGroupInterface $group = NULL, bool $active = TRUE);

  /**
   * Load Memberships by User IDs.
   *
   * @param array $uids
   *   The User IDs.
   * @param \Drupal\mailgroup\Entity\MailGroupInterface|null $group
   *   The Mail Group to filter on. Defaults to NULL, in which case all Mail
   *   Groups are taken into account.
   * @param bool $active
   *   Whether to return active memberships only. Defaults to TRUE.
   *
   * @return \Drupal\mailgroup\Entity\MailGroupMembershipInterface[]
   *   An array of memberships indexed by their ids.
   */
  public function loadByUids(array $uids, MailGroupInterface $group = NULL, bool $active = TRUE);

  /**
   * Load Memberships by User IDs and status.
   *
   * This function has no access checks and is intended for usage in bulk
   * actions only.
   *
   * @param array $uids
   *   The User IDs.
   * @param bool $status
   *   The membership status to filter on.
   * @param int|null $gid
   *   The Mail Group ID to filter on. Defaults to NULL, in which case all
   *   Mail Groups are taken into account.
   *
   * @return \Drupal\mailgroup\Entity\MailGroupMembershipInterface[]
   *   An array of memberships indexed by their ids.
   */
  public function loadByUidsAndStatus(array $uids, bool $status, int $gid = NULL);

}
