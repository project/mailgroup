<?php

namespace Drupal\mailgroup\Entity\Storage;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\mailgroup\Entity\MailGroupInterface;

/**
 * Storage handler for Membership entities.
 */
class MailGroupMembershipStorage extends SqlContentEntityStorage implements MailGroupMembershipStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function checkMembershipsByUids(array $uids, int $gid = NULL) {

    $query = $this->database->select($this->baseTable, 'mm');
    $query->addField('mm', 'uid');
    $query->condition('mm.uid', $uids, 'IN');

    if (is_int($gid)) {
      $query->condition('gid', $gid);
    }

    $result = $query->execute()->fetchCol();
    return array_unique($result);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteByUids(array $uids, int $gid = NULL) {

    $properties = [
      'uid' => $uids,
    ];

    if (is_int($gid)) {
      $properties['gid'] = $gid;
    }

    // In this situation, we don't want 'view' access checks, so we can use
    // loadByProperties().
    $memberships = $this->loadByProperties($properties);
    $this->delete($memberships);

    return count($memberships);
  }

  /**
   * {@inheritdoc}
   */
  public function loadByEmail(string $email, MailGroupInterface $group = NULL, bool $active = TRUE) {

    $properties = [
      'uid.entity:user.mail' => $email,
    ];

    return $this->loadByCustomProperties($properties, $group, $active);
  }

  /**
   * {@inheritdoc}
   */
  public function loadByGroup(MailGroupInterface $group, bool $active = TRUE) {
    return $this->loadByCustomProperties([], $group, $active);
  }

  /**
   * {@inheritdoc}
   */
  public function loadByUid(int $uid, MailGroupInterface $group = NULL, bool $active = TRUE) {
    return $this->loadByUids([$uid], $group, $active);
  }

  /**
   * {@inheritdoc}
   */
  public function loadByUids(array $uids, MailGroupInterface $group = NULL, bool $active = TRUE) {

    $properties = [
      'uid' => $uids,
    ];

    return $this->loadByCustomProperties($properties, $group, $active);
  }

  /**
   * {@inheritdoc}
   */
  public function loadByUidsAndStatus(array $uids, bool $status, int $gid = NULL) {

    $properties = [
      'uid' => $uids,
      'status' => $status,
    ];

    if (is_int($gid)) {
      $properties['gid'] = $gid;
    }

    // In this situation, we don't want 'view' access checks, so we can use
    // loadByProperties().
    /** @var \Drupal\mailgroup\Entity\MailGroupMembershipInterface[] $memberships */
    $memberships = $this->loadByProperties($properties);

    return $memberships;
  }

  /**
   * Custom intermediate for loadByProperties() to reduce code redundancy.
   *
   * @param array $properties
   *   The custom properties to filter on.
   * @param \Drupal\mailgroup\Entity\MailGroupInterface|null $group
   *   A group to load the membership for.
   * @param bool $active
   *   Return active memberships only. Defaults to TRUE.
   *
   * @return \Drupal\mailgroup\Entity\MailGroupMembershipInterface[]
   *   An array of memberships indexed by their ids.
   */
  protected function loadByCustomProperties(array $properties = [], MailGroupInterface $group = NULL, bool $active = TRUE) {

    if ($group instanceof MailGroupInterface) {
      $properties['gid'] = $group->id();
    }

    if ($active === TRUE) {
      $properties['status'] = TRUE;
    }

    // Don't use loadByProperties() as it disables access check, which we do
    // want in this case.
    $entity_query = $this->getQuery();
    $entity_query->accessCheck();
    $this->buildPropertyQuery($entity_query, $properties);
    $result = $entity_query->execute();

    /** @var \Drupal\mailgroup\Entity\MailGroupMembershipInterface[] $memberships */
    $memberships = $result ? $this->loadMultiple($result) : [];
    return $memberships;
  }

}
