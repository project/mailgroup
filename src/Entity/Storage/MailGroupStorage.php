<?php

namespace Drupal\mailgroup\Entity\Storage;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Storage handler for mail group entities.
 */
class MailGroupStorage extends SqlContentEntityStorage implements MailGroupStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function getIdsByConnectionPlugin(string $plugin_id, $active = TRUE) {

    $properties = [
      'connection' => $plugin_id,
    ];

    if ($active) {
      $properties['status'] = $active;
    }

    $entity_query = $this->getQuery();
    $entity_query->accessCheck(FALSE);
    $this->buildPropertyQuery($entity_query, $properties);

    return $entity_query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function loadMailGroups($active = TRUE) {

    if ($active) {
      return $this->loadByProperties(['status' => TRUE]);
    }

    return $this->loadMultiple();
  }

  /**
   * {@inheritdoc}
   */
  public function loadByEmail($email) {
    try {
      $group = $this->loadByProperties(['email' => $email]);
      return reset($group);
    }

    catch (\Exception $e) {
      return FALSE;
    }
  }

}
