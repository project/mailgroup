<?php

namespace Drupal\mailgroup\Entity\Storage;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;

/**
 * Storage handler for Mail Group Type entities.
 */
interface MailGroupTypeStorageInterface extends ConfigEntityStorageInterface {

  /**
   * Get Mail Group Type entities without Encryption Profile.
   *
   * @return \Drupal\mailgroup\Entity\MailGroupTypeInterface[]
   *   An array of Mail Group Type config entities, indexed by their IDs.
   */
  public function loadWithMissingEncryption();

}
