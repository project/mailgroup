<?php

namespace Drupal\mailgroup\Entity\Storage;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Storage handler for mail group entities.
 */
interface MailGroupStorageInterface extends ContentEntityStorageInterface {

  /**
   * Get the IDs of Mail Groups using given Connection plugin.
   *
   * @param string $plugin_id
   *   The Connection plugin ID.
   * @param bool $active
   *   Whether to check active Mail Groups only. Defaults to TRUE.
   *
   * @return array
   *   An array of Mail Groups entity IDs.
   */
  public function getIdsByConnectionPlugin(string $plugin_id, $active = TRUE);

  /**
   * Load all Mail Groups.
   *
   * @param bool $active
   *   Return active Mail Groups only. Defaults to TRUE.
   *
   * @return \Drupal\mailgroup\Entity\MailGroupInterface[]
   *   An array of Mail Groups entities indexed by their IDs.
   */
  public function loadMailGroups($active = TRUE);

  /**
   * Load a group by email address.
   *
   * @param string $email
   *   The group email address.
   *
   * @return \Drupal\mailgroup\Entity\MailGroupInterface|false
   *   The group object, or FALSE if no group could be found.
   */
  public function loadByEmail($email);

}
