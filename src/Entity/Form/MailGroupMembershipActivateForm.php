<?php

namespace Drupal\mailgroup\Entity\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Confirm form for activating a membership.
 */
class MailGroupMembershipActivateForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    /** @var \Drupal\mailgroup\Entity\MailGroupMembershipInterface $membership */
    $membership = $this->getEntity();
    $group = $membership->getGroup();

    return $this->t('Are you sure you want to activate the membership of %email
      in %group?', [
        '%email' => $membership->getOwner()->mail->value,
        '%group' => $group->label(),
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Activate');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $entity = $this->getEntity();
    return $entity->toUrl('canonical');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\mailgroup\Entity\MailGroupMembershipInterface $membership */
    $membership = $this->getEntity();
    $membership
      ->setActive()
      ->save();

    $group = $membership->getGroup();

    $this->messenger()->addMessage(
      $this->t('The membership of %email in %group has been activated.', [
        '%email' => $membership->getOwner()->mail->value,
        '%group' => $group->label(),
      ])
    );
  }

}
