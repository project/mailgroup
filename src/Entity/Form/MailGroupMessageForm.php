<?php

namespace Drupal\mailgroup\Entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for mail group message edit forms.
 */
class MailGroupMessageForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $entity = parent::validateForm($form, $form_state);

    try {
      /** @var \Drupal\mailgroup\Entity\MailGroupMessageInterface $message */
      $message = $this->getEntity();
      $message->getGroup()->isAllowedToSend($message->getSender());
    }
    catch (\Exception $exception) {
      $form_state->setError($form, $exception->getMessage());
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\mailgroup\Entity\MailGroupMessageInterface $message */
    $message = $this->getEntity();

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the message.', [
          '%label' => $message->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the message.', [
          '%label' => $message->label(),
        ]));
    }
    $form_state->setRedirect('entity.mailgroup_message.canonical', [
      'mailgroup' => $message->getGroupId(),
      'mailgroup_message' => $message->id(),
    ]);

    return $status;
  }

}
