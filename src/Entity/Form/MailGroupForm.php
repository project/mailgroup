<?php

namespace Drupal\mailgroup\Entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\mailgroup\ConnectionManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for mail group edit forms.
 */
class MailGroupForm extends ContentEntityForm {

  /**
   * The mail group connection plugin manager.
   *
   * @var \Drupal\mailgroup\ConnectionManager
   */
  protected $connectionManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var self $instance */
    $instance = parent::create($container);

    return $instance
      ->setConnectionManager($container->get('plugin.manager.mailgroup_connection'));
  }

  /**
   * Set the connection plugin manager.
   *
   * @param \Drupal\mailgroup\ConnectionManager $connection_manager
   *   The connection plugin manager.
   *
   * @return $this
   */
  protected function setConnectionManager(ConnectionManager $connection_manager) {
    $this->connectionManager = $connection_manager;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\mailgroup\Entity\MailGroupInterface $group */
    $group = $this->getEntity();

    $form = parent::buildForm($form, $form_state);

    $form['advanced'] = [
      '#type' => 'vertical_tabs',
      '#weight' => 99,
    ];

    $form['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
      '#group' => 'advanced',
    ];

    $settings_fields = [
      'prepend_name',
      'reply_to',
      'send_type',
    ];

    foreach ($settings_fields as $field) {
      $form['settings'][$field] = $form[$field];
      unset($form[$field]);
    }

    $form['connection']['#type'] = 'details';
    $form['connection']['#title'] = $this->t('Connection');
    $form['connection']['#group'] = 'advanced';

    $form['connection']['connection_config'] = [
      '#type' => 'container',
      '#title' => $this->t('Configuration'),
      '#open' => TRUE,
      '#attributes' => ['id' => 'connection-config'],
      '#weight' => 20,
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          'select[name="connection"]' => ['!value' => '_none'],
        ],
      ],
    ];

    $form['connection']['widget']['#ajax'] = [
      'callback' => [$this, 'connectionFields'],
      'event' => 'change',
      'wrapper' => 'connection-config',
      'progress' => [
        'type' => 'throbber',
        'message' => $this->t('Loading fields...'),
      ],
    ];

    $connection = $form_state->getValue('connection');
    if ($connection) {
      $plugin_id = $connection[0]['value'];
      $config = $group->getConnectionConfig();

      /** @var \Drupal\mailgroup\ConnectionInterface $plugin */
      $plugin = $this->connectionManager->createInstance($plugin_id, $config);
    }
    else {
      $plugin = $group->getConnectionPlugin();
    }

    if ($plugin) {
      $fields = $plugin->getFields();
      $form['connection']['connection_config'] += $fields;
    }

    $form['uid']['#type'] = 'details';
    $form['uid']['#title'] = $this->t('Owner');
    $form['uid']['#group'] = 'advanced';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    /** @var \Drupal\mailgroup\Entity\MailGroupInterface $group */
    $group = $this->getEntity();

    // Since password field values might be removed from the form state by
    // validateForm(), we cannot save the form state values as is.
    // As safe approach, replace all the newly submitted config in the current
    // config, and do this recursively to be sure to support whatever nesting
    // might be present.
    $current_values = $group->getConnectionConfig();
    $new_values = $form_state->getValue('connection_config');
    $values = array_replace_recursive($current_values, $new_values);

    $group->setConnectionConfig($values);
    $status = parent::save($form, $form_state);

    if ($status == SAVED_NEW) {
      $this->messenger()->addMessage($this->t('Created the %label group.', [
        '%label' => $group->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('Saved the %label group.', [
        '%label' => $group->label(),
      ]));
    }

    $form_state->setRedirect('entity.mailgroup.collection');

    return $status;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $entity = parent::validateForm($form, $form_state);

    // When we go towards saving the config, first check for non-required empty
    // password fields in the submission, so their current values are not
    // cleared/overwritten with the empty value.
    // This approach assumes that password fields that are not empty in config,
    // are not flagged as required by the connection plugin.
    if (empty($form_state->getErrors())) {

      $config_values = $form_state->getValue('connection_config');

      $children = Element::children($form['connection']['connection_config']);
      foreach ($children as $childname) {

        $element = $form['connection']['connection_config'][$childname];
        if ($element['#type'] === 'password' && $element['#required'] === FALSE) {

          if (empty($config_values[$childname])) {
            unset($config_values[$childname]);
          }
        }
      }

      $form_state->setValue('connection_config', $config_values);
    }

    return $entity;
  }

  /**
   * AJAX callback to add connection plugin fields to the mail group form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public static function connectionFields(array &$form, FormStateInterface $form_state) {
    return $form['connection']['connection_config'];
  }

}
