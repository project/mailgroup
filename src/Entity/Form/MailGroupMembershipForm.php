<?php

namespace Drupal\mailgroup\Entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for group membership edit forms.
 */
class MailGroupMembershipForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\mailgroup\Entity\MailGroupMembershipInterface $membership */
    $membership = $this->getEntity();
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $message = $this->t('Added %user to %group.', [
          '%user' => $membership->getOwner()->getDisplayName(),
          '%group' => $membership->getGroup()->label(),
        ]);
        $this->messenger()->addMessage($message);
        break;

      default:
        $message = $this->t('Updated the group membership for %user.', [
          '%user' => $membership->getOwner()->getDisplayName(),
        ]);
        $this->messenger()->addMessage($message);
    }

    $form_state->setRedirect('entity.mailgroup_membership.collection', [
      'mailgroup' => $membership->getGroupId(),
    ]);

    return $status;
  }

}
