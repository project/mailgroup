<?php

namespace Drupal\mailgroup\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining mail groups.
 */
interface MailGroupInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * Indicates that messages should be sent to members individually.
   */
  const SEND_INDIVIDUAL = 1;

  /**
   * Indicates that messages should be sent with members grouped.
   */
  const SEND_GROUPED = 2;

  /**
   * Indicates that replies should go to all members of a group.
   */
  const REPLY_ALL = 1;

  /**
   * Indicates that replies should go to the sender of the message only.
   */
  const REPLY_SENDER = 2;

  /**
   * Gets the group name.
   *
   * @return string
   *   The group name.
   */
  public function getName();

  /**
   * Sets the group name.
   *
   * @param string $name
   *   The group name.
   *
   * @return $this
   */
  public function setName($name);

  /**
   * Gets the group email address.
   *
   * @return string
   *   The group email address.
   */
  public function getEmail();

  /**
   * Sets the group email address.
   *
   * @param string $email
   *   The group email address.
   *
   * @return $this
   */
  public function setEmail($email);

  /**
   * Gets the prepend name setting.
   *
   * @return bool
   *   The prepend name setting.
   */
  public function prependName();

  /**
   * Sets the prepend name setting.
   *
   * @param bool $prepend
   *   The prepend name setting.
   *
   * @return $this
   */
  public function setPrependName($prepend);

  /**
   * Gets the group reply-to setting.
   *
   * @return string
   *   The group reply-to setting.
   */
  public function getReplyTo();

  /**
   * Sets the group reply-to setting.
   *
   * @param string $reply_type
   *   The group reply-to setting.
   *
   * @return $this
   */
  public function setReplyTo($reply_type);

  /**
   * Gets the group send type.
   *
   * @return string
   *   The group send type.
   */
  public function getSendType();

  /**
   * Sets the group send type.
   *
   * @param string $send_type
   *   The group send type.
   *
   * @return $this
   */
  public function setSendType($send_type);

  /**
   * Gets the group email connection plugin.
   *
   * @return \Drupal\mailgroup\ConnectionInterface|null
   *   The group email connection plugin, or NULL if it has not been set.
   */
  public function getConnectionPlugin();

  /**
   * Sets the group email connection plugin.
   *
   * @param string $plugin
   *   The group email connection plugin.
   *
   * @return $this
   */
  public function setConnectionPlugin($plugin);

  /**
   * Get the connection plugin ID.
   *
   * @return string|null
   *   The connection plugin ID.
   */
  public function getConnectionPluginId();

  /**
   * Gets the group connection configuration.
   *
   * @return array
   *   An array of configuration values.
   */
  public function getConnectionConfig();

  /**
   * Sets the group connection configuration.
   *
   * @param array $config
   *   An array of configuration values.
   *
   * @return $this
   */
  public function setConnectionConfig(array $config);

  /**
   * Returns the group status.
   *
   * @return bool
   *   TRUE if the group is active.
   */
  public function isActive();

  /**
   * Sets the group status to active.
   *
   * @return $this
   */
  public function setActive();

  /**
   * Sets the group status to inactive.
   *
   * @return $this
   */
  public function setInactive();

  /**
   * Test the connection to the group email.
   */
  public function testConnection();

  /**
   * Gets the messages belonging to a group.
   *
   * @return \Drupal\mailgroup\Entity\MailGroupMessageInterface[]
   *   An array of loaded message entities.
   */
  public function getMessages();

  /**
   * Get the member of the group matching given email.
   *
   * @param string $email
   *   The email address of the user to check.
   *
   * @return \Drupal\mailgroup\Entity\MailGroupMembershipInterface|null
   *   The membership entity.
   */
  public function getMember($email);

  /**
   * Gets the members of the group.
   *
   * @return \Drupal\mailgroup\Entity\MailGroupMembershipInterface[]
   *   An array of loaded membership entities.
   */
  public function getMembers();

  /**
   * Gets the email addresses of group members.
   *
   * @return array
   *   An array of member email addresses.
   */
  public function getMemberEmails();

  /**
   * Checks if a user is a member of the group.
   *
   * @param string $email
   *   The email address of the user to check.
   *
   * @return bool
   *   TRUE if the user is a member.
   */
  public function isMember($email);

  /**
   * Checks if a given email is allowed to send to the group.
   *
   * Combines isActive and getMember.
   *
   * @param string $email
   *   The email address of the user to check.
   *
   * @return \Drupal\mailgroup\Entity\MailGroupMembershipInterface|null
   *   The membership entity.
   *
   * @throws \Drupal\mailgroup\Exception\MailGroupInactiveException
   * @throws \Drupal\mailgroup\Exception\MemberNotFoundException
   */
  public function isAllowedToSend($email);

}
