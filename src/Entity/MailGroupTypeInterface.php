<?php

namespace Drupal\mailgroup\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Provides an interface for defining mail group types.
 */
interface MailGroupTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {

  /**
   * Get the Encryption Profile.
   *
   * @return \Drupal\encrypt\EncryptionProfileInterface|null
   *   The Encryption Profile.
   */
  public function getEncryptionProfile();

}
