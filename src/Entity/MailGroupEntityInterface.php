<?php

namespace Drupal\mailgroup\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides a base interface for defining mail group entities.
 */
interface MailGroupEntityInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * Gets the group entity.
   *
   * @return \Drupal\mailgroup\Entity\MailGroupInterface
   *   Group of the membership.
   */
  public function getGroup();

  /**
   * Sets the group entity.
   *
   * @param \Drupal\mailgroup\Entity\MailGroupInterface $group
   *   The group entity.
   *
   * @return $this
   */
  public function setGroup(MailGroupInterface $group);

  /**
   * Gets the group ID.
   *
   * @return int
   *   The group ID.
   */
  public function getGroupId();

  /**
   * Sets the group ID.
   *
   * @param int $gid
   *   The group of the membership.
   *
   * @return $this
   */
  public function setGroupId($gid);

}
