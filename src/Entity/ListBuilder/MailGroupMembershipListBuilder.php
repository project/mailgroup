<?php

namespace Drupal\mailgroup\Entity\ListBuilder;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of group memberships.
 */
class MailGroupMembershipListBuilder extends MailGroupEntityListBuilder {

  /**
   * The group to list members for.
   *
   * @var \Drupal\mailgroup\Entity\MailGroup
   */
  protected $group;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['user'] = $this->t('User');
    $header['email'] = $this->t('Email Address');
    $header['status'] = $this->t('Status');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\mailgroup\Entity\MailGroupMembershipInterface $entity */

    $account = $entity->getOwner();

    $row['user'] = Link::createFromRoute(
      $account->label(),
      'entity.user.canonical',
      ['user' => $account->id()]
    );
    $row['email'] = $account->getEmail();
    $row['status'] = $entity->isActive() ? $this->t('Active') : $this->t('Inactive');

    return $row + parent::buildRow($entity);
  }

}
