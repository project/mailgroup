<?php

namespace Drupal\mailgroup;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Logger\LoggerChannelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Mail group connection plugin base class.
 */
abstract class ConnectionBase extends PluginBase implements ConnectionInterface, ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * The mail group logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return (new static($configuration, $plugin_id, $plugin_definition))
      ->setLogger($container->get('logger.channel.mailgroup'));
  }

  /**
   * Set the mail group logger.
   *
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The mail group logger.
   *
   * @return $this
   */
  protected function setLogger(LoggerChannelInterface $logger) {
    $this->logger = $logger;
    return $this;
  }

  /**
   * Get the current configuration for the plugin.
   *
   * @return array
   *   An array of configuration items.
   */
  public function getConfig() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldsToEncrypt() {

    $definition = $this->getPluginDefinition();
    return $definition['fields_to_encrypt'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function useMailPlugin() {
    return FALSE;
  }

}
