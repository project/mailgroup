<?php

namespace Drupal\mailgroup\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Controller for mail group memberships.
 */
class MailGroupMembershipController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function userPage($user) {
    $rows = [];

    /** @var \Drupal\mailgroup\Entity\Storage\MailGroupMembershipStorageInterface $storage */
    $storage = $this->entityTypeManager()->getStorage('mailgroup_membership');
    $memberships = $storage->loadByUid($user->id(), NULL, FALSE);

    foreach ($memberships as $membership) {
      $group = $membership->getGroup();

      $destination_url = Url::fromRoute('entity.mailgroup_membership.user', [
        'user' => $user->id(),
      ]);
      $destination = $destination_url->toString();

      $operations = [
        '#type' => 'dropbutton',
        '#dropbutton_type' => 'extrasmall',
        '#links' => [],
      ];

      if ($membership->isActive()) {
        $operations['#links']['deactivate'] = [
          'title' => $this->t('Deactivate'),
          'url' => Url::fromRoute('entity.mailgroup_membership.deactivate_form', [
            'mailgroup' => $group->id(),
            'mailgroup_membership' => $membership->id(),
          ], [
            'query' => [
              'destination' => $destination,
            ],
          ]),
        ];
      }
      else {
        $operations['#links']['activate'] = [
          'title' => $this->t('Activate'),
          'url' => Url::fromRoute('entity.mailgroup_membership.activate_form', [
            'mailgroup' => $group->id(),
            'mailgroup_membership' => $membership->id(),
          ], [
            'query' => [
              'destination' => $destination,
            ],
          ]),
        ];
      }

      $operations['#links']['edit'] = [
        'title' => $this->t('Edit'),
        'url' => Url::fromRoute('entity.mailgroup_membership.edit_form', [
          'mailgroup' => $group->id(),
          'mailgroup_membership' => $membership->id(),
        ], [
          'query' => [
            'destination' => $destination,
          ],
        ]),
      ];

      if (
        $this->currentUser()->hasPermission('administer mail groups') ||
        $this->currentUser()->hasPermission('delete mail group memberships') ||
        $this->currentUser()->hasPermission('delete own mail group memberships')
      ) {
        $operations['#links']['delete'] = [
          'title' => $this->t('Delete'),
          'url' => Url::fromRoute('entity.mailgroup_membership.delete_form', [
            'mailgroup' => $group->id(),
            'mailgroup_membership' => $membership->id(),
          ], [
            'query' => [
              'destination' => $destination,
            ],
          ]),
        ];
      }

      $rows[] = [
        'group' => $group->label(),
        'status' => $membership->isActive() ? $this->t('Active') : $this->t('Inactive'),
        'operations' => [
          'data' => $operations,
        ],
      ];
    }

    $build['table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Group'),
        $this->t('Status'),
        $this->t('Operations'),
      ],
      '#rows' => $rows,
      '#empty' => $this->t('You do not have any memberships.'),
    ];

    return $build;
  }

}
