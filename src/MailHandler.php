<?php

namespace Drupal\mailgroup;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\mailgroup\Entity\MailGroupInterface;
use Drupal\mailgroup\Entity\MailGroupMessageInterface;

/**
 * Service for handling mail group mail.
 */
class MailHandler implements MailHandlerInterface {

  use MailgroupMailDerivativeTrait;
  use StringTranslationTrait;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The mail group logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Sets the mail manager.
   *
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager.
   */
  public function setMailManager(MailManagerInterface $mail_manager) {
    $this->mailManager = $mail_manager;
  }

  /**
   * Sets the language manager.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function setLanguageManager(LanguageManagerInterface $language_manager) {
    $this->languageManager = $language_manager;
  }

  /**
   * Sets the mail group logger.
   *
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The mail group logger.
   */
  public function setLogger(LoggerChannelInterface $logger) {
    $this->logger = $logger;
  }

  /**
   * Send a message to members of the group.
   *
   * @param \Drupal\mailgroup\Entity\MailGroupMessageInterface $message
   *   The message to send.
   */
  public function sendMessage(MailGroupMessageInterface $message) {
    $recipients = [];
    $group = $message->getGroup();

    $group_name = $group->getName();
    $group_email = $group->getEmail();
    $emails = $group->getMemberEmails();
    $reply_to = $group->getReplyTo();
    $send_type = $group->getSendType();

    if ($reply_to == MailGroupInterface::REPLY_ALL) {
      $reply = $group_email;
    }
    else {
      $reply = $message->getSender();
    }

    $langcode = $this->languageManager->getCurrentLanguage()->getId();

    $params = [
      'subject' => $message->getSubject(),
      'message' => $message->getBody(),
      'from' => "$group_name <$group_email>",
    ];

    if ($send_type == MailGroupInterface::SEND_INDIVIDUAL) {
      foreach ($emails as $email) {
        $recipients[] = $email;
      }
    }
    else {
      $recipients[] = 'undisclosed-recipients:;';
      $params['bcc'] = implode(', ', $emails);
    }

    $module = 'mailgroup';
    $key = 'group_send';

    $connection = $group->getConnectionPlugin();
    if ($connection->useMailPlugin()) {
      $module = $connection->getPluginDefinition()['provider'];
      $key = $this->buildMailDerivativeKey($connection->getPluginId(), $group->id());
    }

    foreach ($recipients as $to) {
      $result = $this->mailManager->mail($module, $key, $to, $langcode, $params, $reply);
      if ($result['result']) {
        $log_message = $this->t('Message from @group sent to @email.', [
          '@group' => $group_name,
          '@email' => $to,
        ]);
        $this->logger->notice($log_message);
      }
      else {
        $log_message = $this->t('There was a problem sending a message to @email.', [
          '@email' => $to,
        ]);
        $this->logger->error($log_message);
      }
    }
  }

}
