<?php

namespace Drupal\mailgroup;

interface MessageParserInterface {

  /**
   * Create a Mail Group message entity.
   *
   * @return \Drupal\mailgroup\Entity\MailGroupMessageInterface
   *   The message entity.
   *
   * @throws \Drupal\mailgroup\Exception\MailGroupInactiveException
   * @throws \Drupal\mailgroup\Exception\MemberNotFoundException
   */
  public function createMessageEntity();

}
