<?php

namespace Drupal\mailgroup;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * An interface for mail group connection plugins.
 */
interface ConnectionInterface extends PluginInspectionInterface {

  /**
   * Get form fields for configuring the connection.
   *
   * @return array
   *   An array of fields to add to the mail group form.
   */
  public function getFields();

  /**
   * Get configuration fields to store encrypted.
   *
   * @return array
   *   An array of field IDs.
   */
  public function fieldsToEncrypt();

  /**
   * Test the connection to the email server.
   *
   * @return bool
   *   A boolean indicating if the connection was successful.
   */
  public function testConnection();

  /**
   * Check if the connection has a Mail plugin that needs to be used.
   *
   * @return bool
   *   TRUE if the Mail plugin exists and needs to be used to send Mailgroup
   *   Messages for the Mailgroup using this plugin.
   */
  public function useMailPlugin();

}
