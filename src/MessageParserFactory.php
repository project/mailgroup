<?php

namespace Drupal\mailgroup;

use Drupal\mailgroup\Entity\MailGroupInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use ZBateson\MailMimeParser\Message;

/**
 * Factory class for MessageParser instances.
 */
class MessageParserFactory implements ContainerAwareInterface {

  use ContainerAwareTrait;

  /**
   * Create a MessageParser object from a string.
   *
   * @param string $string
   *   A MIME string of the raw email.
   * @param \Drupal\mailgroup\Entity\MailGroupInterface $group
   *   The group this message belongs to.
   *
   * @return \Drupal\mailgroup\MessageParserInterface
   *   The MessageParser object.
   */
  public function createFromString($string, MailGroupInterface $group) {
    $message = Message::from($string, TRUE);

    $parser = (new MessageParser())
      ->setEntityTypeManager($this->container->get('entity_type.manager'))
      ->setGroup($group)
      ->setMessage($message);

    return $parser;
  }

}
